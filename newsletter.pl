#!/usr/bin/perl 

# Seminarnewsletterversender
# Version 1.0
#
# von Robert C. Helling (helling@atdotde.de)
# veroeffentlicht unter der GNU Public License (GPL 3)
#
# Trivialaenderung 2

use strict;

use LWP::Simple;
use Date::Parse;
use Date::Language;
use DateTime;
use Mail::SendEasy;
use Getopt::Std;
use Text::CSV;
use Data::Dumper;
use Term::ReadLine;
use File::Temp qw(tempdir);
use DateTime::Format::Strptime;

use Cwd;
use utf8;

require './TransKeys.pm';

# Konfiguration (will be overwritten by newsletter.config)

#binmode(STDOUT, ":utf8"); 

my $lang = Date::Language->new('English');
# my $lang = Date::Language->new('German');
my $term = Term::ReadLine->new('input');
my $transkey = Term::TransKeys->new();

my $strp = DateTime::Format::Strptime->new(
					   pattern   => '%d.%m.%y %H:%M',
					   on_error  => 'croak',
); 

$|=1;

my %config;

$config{csv_filename} =  "./newsletter.csv";
$config{mailto} = 'robert@atdotde.de';
$config{ASClogo} = '/home/h/helling/ASC.png';
$config{ASCbriefkopf} = '/home/h/helling/ASCbriefkopf.png';
$config{pdfviewer} = 'evince';

if(`hostname -f` =~ /^tmp\./){
   $config{csv_filename} =  "../newsletter.csv";
   $config{mailto} = 'robert@atdotde.de';
   $config{ASClogo} = '/home/robert/ASC.png';
   $config{ASClogo} = '/home/robert/ASCbriefkopf.png';
}
else{
   $ENV{TEXINPUTS} = '/home/h/helling/TeX:';
}

if(-e $ENV{HOME}.'/newsletter.config'){
    open (CONF,$ENV{HOME}.'/newsletter.config') || die "Cannot open ~/newsletter.config:$!";
    while(<CONF>){
	chomp;
	s/#.*//;
	if(/^\s*(\w+)\s*=\s*(\S+)\s*$/){
	    $config{$1} = $2;
#	    print "Setting <$1> to <$2>\n";
	}
    }
}
else{
    print "WARNING: Kein newsletter.config gefunden!\n";
}

my ($montag, $mbemerkungen, $tbemerkungen, @talks, $key);

$montag = DateTime->from_epoch(epoch => time())->truncate(to => 'week')->add(days => 7);
while(1){
    system("clear");
    ($mbemerkungen, $tbemerkungen, @talks) = &read_talks($montag);
    foreach my $talk(@talks){
	print $talk->{sprecher},"\n";
    }
    
    print "\n\nHOCH/RUNTER: Woche aendern, RETURN: akzeptieren, STRG-C: abbrechen\n";

    $key = $transkey->TransKey(0);
#    print "Key pressed: $key\n";
    if($key eq '<CONTROL+C>'){
	exit;
    }
    elsif($key eq '<ENTER>'){
	last;
    }
    elsif($key eq '<UP>'){
	$montag->add(days => -7);
    }
    elsif($key eq '<DOWN>'){
	$montag->add(days => 7);
    }
}
    

my $dienstag = $montag->clone->add(days => 1);
my $sonntag = $montag->clone->add(days => 6);
my $neuersonntag = $montag->clone->add(days => 13);
my $neuermontag = $montag->clone->add(days => 7);
my $woche = $montag->month_name.' '.$montag->day.' - '.$sonntag->month_name.' '.$sonntag->day.', '.$sonntag->year;
my $neuewoche = $neuermontag->month_name.' '.$neuermontag->day.' - '.$neuersonntag->month_name.' '.$neuersonntag->day.', '.$neuersonntag->year;

my $oldpwd = getcwd;
#my $tmpdir = File::Temp->newdir();

my $tmpdir = tempdir(CLEANUP => 1);

chdir $tmpdir;
print "Tempdir ist $tmpdir\n";


my @attachments = ("$tmpdir/newsletter.pdf");

open (NL,">newsletter.tex") || die "Cannot open $tmpdir/newsletter.tex: $!";

print NL "\\nopagenumbers\\input pdfcolor\n\\magnification=\\magstep1\\parindent=0cm\\input graphicx\n".
    "\\def\\nl{\\relax}\n\\font\\gross=cmb10 scaled \\magstep 2\\line{\\gross\\includegraphics[width=6.5cm]".
    "{$config{ASClogo}}\\hss\\vbox to 1.2cm{\\hbox{\\RoyalBlue This Week at the ASC}\\vss\\hbox{$woche}}}\\Black\n\n".
    "\\settabs 2\\columns\n\\overfullrule=0pt\n";


my $body = "Dear Colleague,\n\nPlease find attached the ASC-Newsletter\nfor the week of ".
    $woche.".\n\n".
    "Submission deadline for events taking place in the week of $neuewoche,\nto \"This Week at the ASC\" is Tuesday evening (".
    $dienstag->month_name.' '.$dienstag->day.").\nPlease send your announcements in due time to\n".
    "Ms. Margarita R\"uter-Stimpfle,\nEmail: Rueter-Stimpfle\@physik.uni-muenchen.de\n\nRegards, M. R\"uter-Stimpfle\n\n".
('%' x 55)."\n\n";


if($mbemerkungen){
    $body .= $mbemerkungen.('%' x 55)."\n\n";

    print (('=' x 38)."\n\n". $mbemerkungen);

    print NL "\\medskip\\hrule\\medskip\\centerline{\\Red\\bf NEWS\\Black}\\medskip\n\n{\\bf $tbemerkungen}\n";
}

my $altertag = '';
foreach my $talk (@talks){
    unless($talk->{dt}->day_name eq $altertag){
	$altertag = $talk->{dt}->day_name;
	
	print '=' x 38,"\n";
	print "$altertag, ".$talk->{dt}->dmy('.')."\n\n";
	
	print NL "\\medskip\\vskip 0pt plus 2cm\\goodbreak\\hrule\n\\medskip\n"."$altertag, ".$talk->{dt}->dmy('.')."\n\n\\medskip\n";
	$body .= $altertag.", ".$talk->{dt}->dmy('.')."\n\n";
    }
    
    if($talk->{reihe} =~ /Theory Colloquium|ASC\-PhD\-Colloquium/){
	push @attachments, "$tmpdir/".&colloq_einladung($talk).'.pdf';
    }
    
    print $talk->{reihe}."\n".$talk->{zeit}."h\t".$talk->{msprecher}."\n".
	'Room '.$talk->{raum}."\t". $talk->{maffiliation}."\n".
	$talk->{adresse}."\t".$talk->{titel}."\n\n";
    $body.= $talk->{reihe}."\n".$talk->{zeit}."h\nRoom ".$talk->{raum}."\n".$talk->{madresse}.
	"\nSpeaker: ".$talk->{msprecher}." (".$talk->{maffiliation}.")\nTitle: ".$talk->{mtitel}."\n";
    print NL "{\\bf ".$talk->{reihe}."}\n\n\\+".$talk->{zeit}."h\&{\\bf ".$talk->{tsprecher}."}\\cr\n\\+Room ".
	$talk->{traum}."\&".$talk->{zaffiliation}."\\cr\n\\+\\vtop{\\hsize=0.5\\hsize\\noindent ".$talk->{adresse}."}\&\\vtop{\\raggedright\\hsize=0.5\\hsize\\bf ".$talk->{ttitel};
    if($talk->{hinweis}){
	print NL "\\par {\\rm ".$talk->{thinweis}."}";
	$body .= $talk->{mhinweis}."\n";
    }
    print NL "}\\cr\n\\bigskip\n";

    if($talk->{reihe} =~ /Sommerfeld Theory Colloquium/){
	# print NL "{\\bf Coffee and cookies at ".$talk->{coffee}." in room A348/349.}\n\\bigskip\n\n";
	# $body .= "Coffee and cookies at ".$talk->{coffee}." in room A348/349\n";
    }
    elsif($talk->{titel} =~ /Public Lecture/){
	print NL "{\\bf You are cordially invited to attend the reception after the public lecture!}\n\\bigskip\n\n";
	$body .= "You are cordially invited to attend the reception after the public lecture!\n";
    }
    $body .= "\n";
}

print '=' x 38,"\n";

print NL "\\medskip\\hrule\\medskip\n\\bye\n";
       
$body .= <<EEM;
****************************************************************

Arnold Sommerfeld Center for Theoretical Physics (ASC)

at the Ludwig-Maximilians-Universit"at M"unchen

http://www.asc.physik.lmu.de [www.asc.physik.lmu.de]

Spokesman: Prof. Dieter Luest Tel: +49-89-2180-4372

Scientific Manager: Dr. Michael Haack Tel: +49-89-2180-4377

Secretaries: Ms. Caroline Lesperance Tel: +49-89-2180-4538
             Ms. Margarita Rueter-Stimpfle Tel: +49-89-2180-4641

Email: asc\@theorie.physik.uni-muenchen.de

Fax: +49-89-2180-4186

****************************************************************

EEM

close NL;
print "Newsletter TeXen...\n";
`pdftex newsletter`;
system "$config{pdfviewer} newsletter.pdf &";

#$term->readline('beenden...');

chdir $oldpwd;

my $mail = Mail::SendEasy->new( smtp => 'mail-internal.physik.uni-muenchen.de',
				user => 'rueter-stimpfle',
				pass => '' );

    
print "Mailing to $config{mailto}\n";

my $status = $mail->send(
    from    => 'rueter-stimpfle@physik.uni-muenchen.de' ,
    from_title => 'ASC Newsletter' ,
    reply   => 'asc@theorie.physik.uni-muenchen.de' ,
    error   => 'helling@atdotde.de' ,
    to      => $config{mailto},
    subject => "[ASC] Newsletter ".$woche ,
    msg     => $body,
    msgid   => "ASC-Newsletter-".$montag->epoch ,
    anex   => \@attachments    
    ) ;

if (!$status) { print $mail->error ;}

 $term->readline('beenden...');

sub colloq_einladung {
    my $talk = shift;

    print "Kolloquium \n";

    my ($nachname) = ($talk->{sprecher} =~ /(\w+)\s*$/);
    $nachname = lc($nachname);

    open (COL, ">$nachname.tex") || die "Cannot open $nachname.tex: $!";

    print COL "\\magnification=\\magstephalf\\nopagenumbers\\input pdfcolor\n\\parindent=0cm\\input graphicx\n".
    "\\font\\gross=cmr10 scaled \\magstep 2\\font\\huge=cmb10 scaled \\magstep4\n\\font\\groesser=cmr10 scaled \\magstep1\n".
    "\\def\\fn#1#2{\\footnote{\$^{#1}\$}{\\rm #2}}\n".
    "\\def\\nl{\\hss\\egroup\\line\\bgroup\\hss}".
    "\\vskip 1cm\\line{\\gross\\includegraphics[width=\\hsize]".
    "{$config{ASCbriefkopf}}\\hss}\\Black\n\n".
    "\\vfill\n\\centerline{\\huge Sommerfeld Theory Colloquium}\n\\vskip 1cm\n\\gross\n\\centerline{".
    $talk->{anrede}.' '.$talk->{tsprecher}."}\n\\bigskip\\centerline{\\Green ".$talk->{taffiliation}."\\Black}\n\\bigskip\n".
    "\\centerline{".$talk->{ttitel}."}\n\\bigskip{\\parskip=3mm plus 5mm minus 5mm\\narrower\\noindent ".$talk->{tabstract}.
    "\\par}\\vskip 2cm plus 1cm minus 1cm\n\\Green \\centerline{\\groesser ".
    $talk->{dt}->day_name.", ".$talk->{dt}->day.' '.$talk->{dt}->month_name.' '.$talk->{dt}->year.', '.$talk->{zeit}.'h, Room '.
    $talk->{raum}.', '.$talk->{adresse}."}\n\\vfill\n\\Black\\rm\n\\vskip 1cm plus 1fill minus 9mm\\noindent  ".$talk->{tbemerkung}."\\par\\eject\\end\n";

    close COL;
    print "TeXen\n";
    `pdftex $nachname`;
    system "$config{pdfviewer} $nachname.pdf\&";

    my $colmail = "Dear Colleague,\n\nThe next Sommerfeld Theory Colloquium will be given by:\n\nSpeaker: ".$talk->{msprecher}.' ('.$talk->{maffiliation}.")\n\nTitle: ".
	$talk->{mtitel}."\n\nDate: ".$talk->{dt}->day.' '.$talk->{dt}->month_name.' '.$talk->{dt}->year."\n\nTime: ".$talk->{zeit}."h\n\nLocation: ".
	$talk->{raum}.', '.$talk->{adresse}."\n\nCoffee and cookies at ".$talk->{coffee}." in room 348/349.\n\n\nAbstract:\n".$talk->{mabstract}.
	"\n\nPlease find enclosed the Colloquium announcement in pdf-format.\n\nBest regards,\n\nM. R\"uter-Stimpfle\n\n";
    $colmail .= <<COLFOOTER;
****************************************************************
Arnold Sommerfeld Center for Theoretical Physics (ASC)
at the Ludwig-Maximilians-Universit"at M"unchen
http://www.asc.physik.lmu.de
Spokesman: Prof. Dieter L"ust              Tel: +49-89-2180-4372
Scientific Manager: Dr. Michael Haack      Tel: +49-89-2180-4377
Secretaries: Ms. Caroline Lesperance       Tel: +49-89-2180-4538
             Ms. Margarita R"uter-Stimpfle Tel: +49-89-2180-4641
Email: asc\@theorie.physik.uni-muenchen.de  Fax: +49-89-2180-4186
****************************************************************
COLFOOTER


   my $mailer = Mail::SendEasy->new( smtp => 'mail-internal.physik.uni-muenchen.de',
				      user => 'rueter-stimpfle',
				      pass => '' );

    
    my $status = $mailer->send(
	from    => 'rueter-stimpfle@physik.uni-muenchen.de' ,
	from_title => 'Margarita Rueter-Stimpfle',
	reply   => 'asc@theorie.physik.uni-muenchen.de' ,
	error   => 'helling@atdotde.de' ,
	to      => $config{mailto},
	subject =>  '[ASC] ASC Colloquium, '.$talk->{sprecher}.', '.$talk->{dt}->day.'.'.$talk->{dt}->month.'.'.$talk->{dt}->year.' / '.$talk->{zeit}.'h'  ,
	msg     => $colmail,
	msgid   => "ASC-Colloquium-".$montag->epoch ,
	anex   => "$nachname.pdf"   
    ) ;

    if (!$status) { print $mailer->error ;}

    $colmail =~ s/The next Sommerfeld Theory Colloquium will be given by:/Do not miss tomorrow's Sommerfeld Theory Colloquium!/s;
    $status = $mailer->send(
	from    => 'rueter-stimpfle@physik.uni-muenchen.de' ,
	from_title => 'Margarita Rueter-Stimpfle',
	reply   => 'asc@theorie.physik.uni-muenchen.de' ,
	error   => 'helling@atdotde.de' ,
	to      => $config{mailto},
	subject =>  '[ASC] Tomorrow: ASC Colloquium, '.$talk->{sprecher}.', '.$talk->{dt}->day.'.'.$talk->{dt}->month.'.'.$talk->{dt}->year.' / '.$talk->{zeit}.'h'  ,
	msg     => $colmail,
	msgid   => "ASC-Colloquium-".$montag->epoch ,
	anex   => "$nachname.pdf"   
    ) ;

    if (!$status) { print $mailer->error ;}
    
    return($nachname);
}

sub translate{
    my ($talk, @items) = @_;

    foreach my $ding(@items){
	$talk->{'t'.$ding} = $talk->{$ding};

	$talk->{'t'.$ding} =~ s/ö/\\"o/g;
	$talk->{'t'.$ding} =~ s/ä/\\"a/g;
	$talk->{'t'.$ding} =~ s/ü/\\"u/g;
	$talk->{'t'.$ding} =~ s/ß/\\ss /g;
	$talk->{'t'.$ding} =~ s/Ö/\\"O/g;
	$talk->{'t'.$ding} =~ s/Ä/\\"A/g;
	$talk->{'t'.$ding} =~ s/Ü/\\"U/g;
	$talk->{'t'.$ding} =~ s/”/"/g;
	$talk->{'z'.$ding} = $talk->{'t' . $ding};
	$talk->{'t'.$ding} =~ s/\\\\/\\nl /g;
	$talk->{'z'.$ding} =~ s/\\\\/\\cr\\+&/g;

	$talk->{'m'.$ding} = $talk->{$ding};
	$talk->{'m'.$ding} =~ s/\\\\//g;
	$talk->{'m'.$ding} =~ s/\\-//g;
	$talk->{'m'.$ding} =~ s/\\bf//g;
	$talk->{'m'.$ding} =~ s/\\par//g;
	$talk->{'m'.$ding} =~ s/\\fn\{([^}]*)\}\{([^}]*)\}/[$2]/g;
	$talk->{'m'.$ding} =~ s/{//g;
	$talk->{'m'.$ding} =~ s/}//g;
	$talk->{'m'.$ding} =~ s/ö/oe/g;
	$talk->{'m'.$ding} =~ s/ä/ae/g;
	$talk->{'m'.$ding} =~ s/ü/ue/g;
	$talk->{'m'.$ding} =~ s/ß”/ss/g;
	$talk->{'m'.$ding} =~ s/Ö/Oe/g;
	$talk->{'m'.$ding} =~ s/Ä/Ae/g;
	$talk->{'m'.$ding} =~ s/Ü/Ue/g;
	$talk->{'m'.$ding} =~ s/”/"/g;
	$talk->{'m'.$ding} =~ s/\\par\s*/ /g;
	$talk->{'m'.$ding} =~ s/\\_/_/g;
	$talk->{'m'.$ding} =~ s/\\"([aouAOU])/$1e/g;
	
    }
}

sub read_talks{
    
    my $montag = shift;

#    my $anker = $term->readline("Newsletter fuer die Woche des >");
#    my $montag = DateTime->from_epoch(epoch => $lang->str2time($anker))->truncate(to => 'week');

    my $dienstag = $montag->clone->add(days => 1);
    my $sonntag = $montag->clone->add(days => 6);
    my $neuersonntag = $montag->clone->add(days => 13);
    my $neuermontag = $montag->clone->add(days => 7);
    my $woche = $montag->month_name.' '.$montag->day.' - '.$sonntag->month_name.' '.$sonntag->day.', '.$sonntag->year;
    my $neuewoche = $neuermontag->month_name.' '.$neuermontag->day.' - '.$neuersonntag->month_name.' '.$neuersonntag->day.', '.$neuersonntag->year;
    
    print "Newsletter ".$montag->month_name.' '.$montag->day.' - '.$sonntag->month_name.' '.$sonntag->day.', '.$sonntag->year."\n";
    
    my $csv = Text::CSV->new ( { binary => 1 } )  # should set binary attribute.
	or die "Cannot use CSV: ".Text::CSV->error_diag ();
    
    open (my $fh, "<:encoding(utf8)", $config{csv_filename}) or die "$config{csv_filename}: $!";
    
    $csv->column_names(qw(datum zeit anrede sprecher affiliation reihe titel raum adresse abstract bemerkung hinweis));
    
    <$fh>;
    
    my @talks;
    
    my $mbemerkungen = '';
    my $tbemerkungen = '';

    while(my $talk = $csv->getline_hr($fh)){
	next unless $talk->{datum} =~ /\S/;
	#print "Datum: ".$talk->{datum}." Zeit: ".$talk->{zeit}."\n";
	#print '<'.$talk->{datum}.' '.$talk->{zeit}.'>';
	$talk->{zeit} ||= '9:00';
	#	print Dumper($talk);
	#	$talk->{dt} = DateTime->from_epoch(epoch => str2time($talk->{datum}.' '.$talk->{zeit}));
	$talk->{dt} = $strp->parse_datetime($talk->{datum}.' '.$talk->{zeit});
	unless (ref $talk->{dt}) {
	  print "Cannot parse $talk->{datum} $talk->{zeit}!\n";
	  next;
	}

	next if $talk->{dt} < $montag or $talk->{dt} > $sonntag;
	print "Sprecher: $talk->{sprecher} am ", &datum($talk->{dt}), "\n";

	my $coffee = $talk->{dt}->clone()->add(minutes => -15);
	$coffee->set_time_zone('local');
	$talk->{coffee} = $coffee->hour() .':'.substr("0".$coffee->minute(),-2);
	$talk->{reihe} =~ s/GEO/Geometry/;
	$talk->{reihe} =~ s/FS/Fields and Strings/;
	$talk->{reihe} =~ s/\bCOL\b/Sommerfeld Theory Colloquium/;
	$talk->{reihe} =~ s/\bMP\b/Mathematical Physics/;
	$talk->{reihe} =~ s/\bAMP\b/Analysis and Mathematical Physics/;
	$talk->{reihe} =~ s/\bANA\b/Analysis/;
	$talk->{reihe} =~ s/SOST/Solid State Theory/;
	$talk->{reihe} =~ s/PHDCOL/ASC-PhD-Colloquium/;
	
	$talk->{adresse} =~ s/T37/Theresienstr. 37/;
	$talk->{adresse} =~ s/T39/Theresienstr. 39/;
	$talk->{adresse} =~ s/\s+$/ /;
	$talk->{raum} =~ s/\s+$//;
	
	$talk->{anrede} =~ s/\bP\b/Prof./;
	$talk->{anrede} =~ s/\bD\b/Dr./;
	
	$talk->{raum} =~ s/\s//g;
	#$talk->{adresse} =~ s/\s//g;
	&translate($talk,qw(sprecher affiliation titel abstract bemerkung hinweis adresse raum adresse));
	
	
	unless($talk->{reihe} eq 'BEM'){
	    push @talks,$talk;
	}
	else{
	    $mbemerkungen .= $talk->{mabstract}. "\n\n";
	    $tbemerkungen .= $talk->{tabstract}. "\n\n";
	}

    }
    
    
    return($mbemerkungen,$tbemerkungen, sort {$a->{dt} <=> $b->{dt}} @talks);
}

sub datum {
  my ($dt) = shift;
  return "" unless ref $dt;
  return $dt->day . '.' . $dt->month_name . ' ' . $dt->year;
}
